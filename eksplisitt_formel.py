
import math
import logging
import numpy as np
import matplotlib.pyplot as plt

class Rekker:
    def __init__(self) -> None:
        pass

    def find_constant_diff(self, input_list:list):
        num_iterations = 0
        constant_diff = False
        prev_list = input_list
        
        constant_diff = all(element == input_list[0] for element in input_list)
        if not constant_diff:
            while(not constant_diff):

                diff_list = []

                for i in range(len(prev_list) - 1):

                    diff_list.append(prev_list[i+1] - prev_list[i])

                num_iterations += 1

                constant_diff = all(element == diff_list[0] for element in diff_list)
                if constant_diff:
                    n = num_iterations
                    x = diff_list[0]/math.factorial(n)

                    return x, n
                
                prev_list = []
                prev_list[:] = diff_list[:]
        else:
            return input_list[0], num_iterations
        

    def create_list_from_formula(self, coordinates:list, list_length:int) -> list:
        """Creates a list based on the previous found parts of the formula

        :param coordinates: list of touples containing x and n value
        :type coordinates: list
        :param list_length: The length of the list to create
        :type list_length: int
        :return: List of values calculated from formula
        :rtype: list
        """
        l = np.zeros((len(coordinates), list_length))
        yindex = 0
        for x,n in coordinates:
            for i in range(1, list_length + 1):
                l[yindex][i-1] = (x*(i**n))
            yindex += 1

        if len(l) > 1:
            for i in range(1, len(l)):
                sum_list = self.sum_of_lists(l[i-1], l[i])
        else: 
            sum_list = l[0][:]

        return list(sum_list)


    def sum_of_lists(self, list1, list2) -> list:
        l = []
        logging.debug('lists %s %s', list1, list2)
        for i in range(len(list1)):
            l.append(list1[i] + list2[i])
        return l


    def difference_between_lists(self, original_list, new_list) -> list:
        l = []
        for i in range(len(original_list)):
            l.append(original_list[i] - new_list[i])
        return l

    def pretty_math_output(self, coordinates:list) -> str:
        formula = ''
        coord_num = 0
        for coord in coordinates:
            if coord_num == 0 or coord[0] < 0:
                formula += f' {coord[0]}x^{coord[1]}'
            elif coord[0] >= 0:
                formula += f'+ {coord[0]}x^{coord[1]}'

            coord_num += 1
            
        return formula

    def plot(self, coords, formel) -> None:

        x = np.linspace(-20,20, 1000)
        y = np.zeros(len(x))

        for i in range(len(x)):
            y[i] = self.f(x[i], coords)

        fig = plt.figure(formel)
        ax = fig.add_subplot(1, 1, 1)
        ax.spines['left'].set_position('center')
        ax.spines['bottom'].set_position('zero')
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        plt.plot(x, y, 'r')

        plt.show()

    def f(self, x, coords):
        y = 0
        for i in range(len(coords)):
            y += (coords[i][0]*(x**coords[i][1]))
        return y

    def main(self, original_list):
        loop = True
        i = 0
        prev_list = []
        prev_list[:] = original_list[:]

        generated_coordinates = []
        
        while(loop):
            
            generated_coordinates.append(self.find_constant_diff(prev_list))

            generated_list = self.create_list_from_formula(generated_coordinates, len(original_list))

            if generated_list == original_list:
                loop = False
            elif i >= 10:
                loop = False
            else:
                prev_list[:] = self.difference_between_lists(prev_list, generated_list)
                i += 1
                logging.debug("differanse mellom forrige liste og generert %s", prev_list)
        
        return generated_coordinates


if __name__ == "__main__":

    logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)

    rekker = Rekker()

    generated_coordinates = rekker.main([-4.5, -3, -0.5, 3])
    
    
    formel = rekker.pretty_math_output(generated_coordinates)
    logging.info(formel)
    rekker.plot(generated_coordinates,formel)